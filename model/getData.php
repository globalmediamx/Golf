<?php
	//header("Access-Control-Allow-Origin: *");
	//header("Content-Type: application/json; charset=UTF-8");

	$servername = "localhost";
	$username = "root";
	$password = "";

	// Create connection
	$conn = new mysqli($servername, $username, $password);

	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 
	/*
		SELECT  
			 A.*, 
			 B.*
		FROM 
			 team 
			 LEFT JOIN jugador A ON A.id_player=team.player1 
			 LEFT JOIN jugador B ON B.id_player=team.player2
	*/
	$sql = "SELECT A.*, B.name as 'nameP', B.app as 'appP', B.apm as 'apmP', B.mail as 'mailP', B.phone as 'phoneP', B.handicap as 'handicapP', B.payDate as 'payDateP', B.bank as 'bankP', B.routeServer as 'routeServerP', B.team as 'teamP' FROM golf.team INNER JOIN golf.jugador A ON A.id_player=team.player1 LEFT JOIN golf.jugador B ON B.id_player=team.player2";
	$result = $conn->query($sql);
	$rawdata = array();
	$i=0;

	$outp = "";
	while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
		$rawdata[$i]=$rs;
		$i++;
	}
	//$outp ='{"records":['.$outp.']}';
	//$outp=json_encode($outp);
	$conn->close();
	echo json_encode($rawdata);
?>
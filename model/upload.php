<?php
if ( !empty( $_FILES ) ) {
	
    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
    $uploadPath = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR .date("Y-m-dhmsa"). $_FILES[ 'file' ][ 'name' ];
    move_uploaded_file( $tempPath, $uploadPath );
    $answer = array( 'answer' => 'File transfer completed','routeServer'=> $uploadPath );
    $json = json_encode( $answer );
    echo $json;
	
} else {
    echo 'No files';
}
?>
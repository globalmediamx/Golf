angular.module('golfApp', ['ngRoute','bootstrap.fileField','angularFileUpload']).
controller('main', function($scope,$http,FileUploader) {
	$scope.modal=false;
	$scope.modalSuccess=false;
	$scope.modalError=true;
	$scope.tituloAviso="AVISO";
	$scope.aviso=" Recuerda que antes de hacer tu registro tienes que hacer tu deposito\r\n de $2,900 por concepto de inscripción a la cuenta:\r\n \r\n NEWSPOT MEXICO, S.A. DE C.V. \r\n BANCOMER \r\n CUENTA:  01 54 74 09 04 \r\n CLABE: 012 700 001 547 409 040 \r\n\r\n Y tener a la mano los datos correctos de tu pareja de torneo \r\n  para que el sistema pueda hacer el equipo en cuanto tu pareja se registre. \r\n\r\n Nota: Cada participante tiene que hacer su registro individualmente. "
	var uploader = $scope.uploader = new FileUploader({
            url: 'model/upload.php'
        });
	
	/*EVENTOS DE LA CARGA DEL ARCHIVO*/
	uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
	};
	uploader.onAfterAddingFile = function(fileItem) {
	};
	uploader.onAfterAddingAll = function(addedFileItems) {
	};
	uploader.onBeforeUploadItem = function(item) {
	};
	uploader.onProgressItem = function(fileItem, progress) {
	};
	uploader.onProgressAll = function(progress) {
	};
	uploader.onSuccessItem = function(fileItem, response, status, headers) {
	};
	uploader.onErrorItem = function(fileItem, response, status, headers) {
	};
	uploader.onCancelItem = function(fileItem, response, status, headers) {
	};
	uploader.onCompleteItem = function(fileItem, response, status, headers) {
		$scope.user.routeImg=response.routeServer;
		console.info($scope.user);
		$http.post('model/register.php',$scope.user).
		success(function(data){
			$scope.modal=false;
			$scope.modalSuccess=true;
			if(data=="Error")
			{
				$scope.modalSuccess=false;
				alert("Lo sentimos la pareja que estas intentando registrar ya esta registrado con otro competidor.");
			}
			console.log(data);
		}).error(function(data){
			console.log('Error: '+ data);
		});
	};
	uploader.onCompleteAll = function() {
	};
	/*FIN EVENTOS DE LA CARGA DEL ARCHIVO*/
	
	
    $scope.enviar=function(){
		console.log($scope.user);
		if($scope.user==null||$scope.user.name ==null || $scope.user.mail==null || $scope.user.phone==null || $scope.user.handicap==null || $scope.user.payDate==null||$scope.user.bank==null || $scope.user.uploadFile==null||$scope.user.apP==null||$scope.user.apM==null||$scope.user.apPP==null||$scope.user.apMP==null||$scope.user.handicapP==null||$scope.user.name =="" ||$scope.user.apP==""||$scope.user.apM==""||$scope.user.apPP==""||$scope.user.apMP==""|| $scope.user.mail=="" || $scope.user.phone=="" || $scope.user.handicap=="" || $scope.user.payDate==""||$scope.user.bank==""||$scope.user.handicapP==""){
			if($scope.user==null||$scope.user.mail==null||$scope.user.mail==""){
				$scope.tituloAviso="Error";
				$scope.aviso="Por favor, llene correctamente el campo de e-mail";
				$scope.modalError=true;	
			}else{
				$scope.tituloAviso="Error";
				$scope.aviso="Por favor, llene correctamente todos los campos";
				$scope.modalError=true;
			}
		}
		else{
			$scope.modal=true;
		}
	}
	
	$scope.cancelar=function(){
		$scope.modal=false;
	}
	
	$scope.registrar=function(){
	}
	$scope.fin=function(){
		$scope.modalSuccess=false;
		$scope.user={};
	}
	$scope.finError=function(){
		$scope.modalError=false;
	}
}).
controller('adminCtrl', function($scope,$http) {
	$scope.admin={};
	$scope.loged=true;
	$scope.data=false;
	$scope.galeria=false;
	
	$scope.login=function(){
		$http.post('model/login.php',$scope.admin).
			success(function(data){
				if(data=="true"){
					$scope.loged=false;
					$http.get("model/getData.php")
    					.then(function (response) {
						console.log(response);
						$scope.players = response.data;
						$scope.data=true;
					});
				}else{
					alert("Usuario o contraseña invalidos");
				}
			}).error(function(data){
				console.log('Error: '+ data);
			});
	}
	
	$scope.watch=function(ruta){
		$scope.imagenPago=ruta;
		console.log(ruta);
		$scope.galeria=true;
	}
	
	$scope.closeGalerie=function(){
		$scope.galeria=false;
	}
	
}).
config(function($routeProvider) {
		$routeProvider.
		  when('/', {
			templateUrl: 'templates/main.html',
			controller: 'main'
		  }).
			when('/admin', {
			templateUrl: 'templates/admin.html',
			controller: 'adminCtrl'
		  }).
		  otherwise({
			redirectTo: '/'
		  });
});